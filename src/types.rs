//! Types used as part of REST API calls

#[cfg(feature = "chrono")]
use chrono::{DateTime, Duration, FixedOffset, Local, NaiveDate, NaiveTime, Utc};
use serde_json::Value;
use std::convert::TryFrom;
use std::fmt;

/// Error returned when a value cannot be parsed
#[derive(Clone)]
pub struct ParseError(String);

impl ParseError {
    fn invalid_type(value_type: &str, value: &Value) -> Self {
        ParseError(format!(
            "cannot convert value to {}: {:?}",
            value_type, value
        ))
    }
}

impl fmt::Debug for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Parse a value retrieved via REST API
pub trait Parse {
    fn parse(value: Value) -> Result<Self, ParseError>
    where
        Self: Sized;
}

/// Parse `Option<_>`
///
/// ```
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert_eq!(Option::<i64>::parse(Value::Null).unwrap(), None);
/// assert_eq!(Option::<i64>::parse(Value::Number(124.into())).unwrap(), Some(124_i64));
/// ```
///
/// # String Input
///
/// String input is treated specially, an empty String is considered [`None`]:
///
/// ```
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert_eq!(Option::<String>::parse(Value::Null).unwrap(), None);
/// assert_eq!(Option::<String>::parse(Value::String("".to_string())).unwrap(), None);
/// assert_eq!(Option::<String>::parse(Value::String("content".to_string())).unwrap().unwrap(), "content");
/// ```
///
/// Tocco considers empty string fields unset. In Rust where one can't forget
/// to handle `null`, it's often easier to have an `Option<_>`. Thus, this is
/// is provided. Use `String` if you prefer to have an empty field represented
/// as an empty string.
///
/// There is other field types than strings which are represented as strings
/// also. For instance, duration and datetime. Those can't be empty though
/// and the case of the input being null is still handled. So, this shouldn't
/// interfere.
impl<T> Parse for Option<T>
where
    T: Parse,
{
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::Null => Ok(None),
            Value::String(s) if s.is_empty() => Ok(None),
            value => <T as Parse>::parse(value).map(Some),
        }
    }
}

impl Parse for String {
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::String(value) => Ok(value),
            _ => Err(ParseError::invalid_type("String", &value)),
        }
    }
}

/// Parse value
///
/// ```
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert_eq!(<bool as Parse>::parse(Value::Bool(false)).unwrap(), false);
/// assert_eq!(<bool as Parse>::parse(Value::Bool(true)).unwrap(), true);
/// ```
impl Parse for bool {
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::Bool(value) => Ok(value),
            _ => Err(ParseError::invalid_type("bool", &value)),
        }
    }
}

macro_rules! parse_impl {
    ($type:ident, $into_func:ident, $($test_value:expr),*) => {
        /// Parse value
        ///
        /// ```
        /// use serde_json::{Number, Value};
        /// use tocco::types::Parse;
        ///
        $(
        #[doc = concat!("let val:", stringify!($type), " = Parse::parse(Value::Number(Number::from(", stringify!($test_value), "))).unwrap();")]
        #[doc = concat!("assert_eq!(val, ", stringify!($test_value), ");")]
        )*
        /// ```
        impl Parse for $type {
            fn parse(value: Value) -> Result<Self, ParseError> {
                match value {
                    Value::Number(value) => {
                        let value = value.$into_func().ok_or_else(||
                            ParseError(format!("value {} out of range", value))
                        )?;
                        let value = TryFrom::try_from(value).map_err(|_| {
                            ParseError(format!("value {} out of range", value))
                        })?;
                        Ok(value)
                    }
                    v => Err(ParseError::invalid_type(stringify!($type), &v)),
                }
            }
        }
    };
}

parse_impl!(i8, as_i64, 5_i8, -5_i8);
parse_impl!(i16, as_i64, 16, 5_i16, -5_i16);
parse_impl!(i32, as_i64, 32, 5_i32, -5_i32);
parse_impl!(i64, as_i64, 64, 5_i64, -5_i64);
parse_impl!(u8, as_u64, 5_u8);
parse_impl!(u16, as_u64, 16, 5_u16);
parse_impl!(u32, as_u64, 32, 5_u32);
parse_impl!(u64, as_u64, 64, 5_u64);

/// Parse value
///
/// ```
/// use serde_json::{Number, Value};
/// use tocco::types::Parse;
///
/// let val: f64 = Parse::parse(Value::Number(Number::from_f64(5.6).unwrap())).unwrap();
/// assert_eq!(val, 5.6_f64);
/// ```
impl Parse for f64 {
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::Number(value) => {
                let value = value
                    .as_f64()
                    .ok_or_else(|| ParseError(format!("value {} out of range", value)))?;
                Ok(value)
            }
            _ => Err(ParseError::invalid_type("f64", &value)),
        }
    }
}

/// Parse time
///
/// ```
/// use chrono::NaiveTime;
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert!(<NaiveTime as Parse>::parse(Value::String("08:12:52".to_string())).is_ok());
/// assert!(<NaiveTime as Parse>::parse(Value::String("08:12:52.123".to_string())).is_ok());
/// ```
#[cfg(feature = "chrono")]
impl Parse for NaiveTime {
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::String(value) => NaiveTime::parse_from_str(&value, "%H:%M:%S%.f")
                .map_err(|e| ParseError(format!("Failed to parse value {:?}: {}", value, e))),
            _ => Err(ParseError::invalid_type("NaiveTime", &value)),
        }
    }
}

/// Parse date
///
/// ```
/// use chrono::NaiveDate;
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert!(<NaiveDate as Parse>::parse(Value::String("2000-01-02".to_string())).is_ok());
/// ```
#[cfg(feature = "chrono")]
impl Parse for NaiveDate {
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::String(value) => NaiveDate::parse_from_str(&value, "%Y-%m-%d")
                .map_err(|e| ParseError(format!("Failed to parse value {:?}: {}", value, e))),
            _ => Err(ParseError::invalid_type("NaiveDate", &value)),
        }
    }
}

/// Parse datetime
///
/// ```
/// use chrono::{DateTime, FixedOffset};
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert!(<DateTime<FixedOffset> as Parse>::parse(Value::String("2000-01-02T11:22:35.123Z".to_string())).is_ok());
/// ```
#[cfg(feature = "chrono")]
impl Parse for DateTime<FixedOffset> {
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::String(value) => DateTime::parse_from_rfc3339(&value)
                .map_err(|e| ParseError(format!("Failed to parse value {:?}: {}", value, e))),
            _ => Err(ParseError::invalid_type("DateTime", &value)),
        }
    }
}

/// Parse datetime
///
/// ```
/// use chrono::{ DateTime, Utc};
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert!(<DateTime<Utc> as Parse>::parse(Value::String("2000-01-02T11:22:35.123Z".to_string())).is_ok());
/// ```
#[cfg(feature = "chrono")]
impl Parse for DateTime<Utc> {
    fn parse(value: Value) -> Result<Self, ParseError> {
        let datetime: DateTime<FixedOffset> = Parse::parse(value)?;
        Ok(datetime.into())
    }
}

/// Parse datetime
///
/// ```
/// use chrono::{ DateTime, Local };
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// assert!(<DateTime<Local> as Parse>::parse(Value::String("2000-01-02T11:22:35.123Z".to_string())).is_ok());
/// ```
#[cfg(feature = "chrono")]
impl Parse for DateTime<Local> {
    fn parse(value: Value) -> Result<Self, ParseError> {
        let datetime: DateTime<FixedOffset> = Parse::parse(value)?;
        Ok(datetime.into())
    }
}

/// Parse datetime
///
/// ```
/// use chrono::Duration;
/// use serde_json::Value;
/// use tocco::types::Parse;
///
/// let duration = <Duration as Parse>::parse(Value::Number(3600000.into())).unwrap();
/// assert_eq!(duration, Duration::hours(1));
/// ```
#[cfg(feature = "chrono")]
impl Parse for Duration {
    fn parse(value: Value) -> Result<Self, ParseError> {
        match value {
            Value::Number(value) => {
                let value = value
                    .as_i64()
                    .ok_or_else(|| ParseError(format!("value {} out of range", value)))?;
                Ok(Duration::milliseconds(value))
            }
            _ => Err(ParseError::invalid_type("Duration", &value)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::Number;
    use std::str::FromStr;

    macro_rules! parse_test {
        ($type:ty, $test_name:ident, $invalid_value:expr) => {
            #[test]
            fn $test_name() {
                let result: Result<$type, _> = Parse::parse(Value::from($invalid_value));
                assert!(
                    format!("{:?}", result.unwrap_err()).starts_with("cannot convert value to ",)
                )
            }
        };
    }

    parse_test!(Option<bool>, test_invalid_option_bool, 12);
    parse_test!(bool, test_invalid_bool, 12);
    parse_test!(f64, test_invalid_f64, true);
    parse_test!(String, test_invalid_string, 12);
    #[cfg(feature = "chrono")]
    parse_test!(NaiveTime, test_invalid_naive_time, 12);
    #[cfg(feature = "chrono")]
    parse_test!(NaiveDate, test_invalid_naive_date, 12);
    #[cfg(feature = "chrono")]
    parse_test!(
        DateTime<FixedOffset>,
        test_invalid_naive_date_fixe_offset,
        12
    );
    #[cfg(feature = "chrono")]
    parse_test!(DateTime<Utc>, test_invalid_date_time_utc, 12);
    #[cfg(feature = "chrono")]
    parse_test!(DateTime<Local>, test_invalid_date_time_local, 12);
    #[cfg(feature = "chrono")]
    parse_test!(Duration, test_invalid_duration, "invalid");

    #[test]
    fn parse_bool_err() {
        let result: Result<bool, _> = Parse::parse(Value::from(12));
        assert_eq!(
            format!("{:?}", result.unwrap_err()),
            "cannot convert value to bool: Number(12)"
        );
    }

    macro_rules! parse_integer_test {
        ($type:ident, $test_name:ident$(, $oof_value:expr)*$(,)?) => {
            #[test]
            fn $test_name() {
                let result: Result<$type, _> = Parse::parse(Value::from("test"));
                assert_eq!(
                    format!("{:?}", result.unwrap_err()),
                    concat!("cannot convert value to ", stringify!($type), ": String(\"test\")")
                );

                let result: Result<$type, _> = Parse::parse(Value::Number(
                    Number::from_str("123456789012345678901234567890").unwrap(),
                ));
                assert_eq!(
                    format!("{:?}", result.unwrap_err()),
                    "value 123456789012345680000000000000 out of range"
                );

                $(
                    let result: Result<$type, _> = Parse::parse(Value::from($oof_value));
                    assert_eq!(
                        format!("{:?}", result.unwrap_err()),
                        format!("value {} out of range", $oof_value)
                    );
                )*
            }
        };
    }

    parse_integer_test!(i8, parse_i8_err, -129, 128);
    parse_integer_test!(i16, parse_i16_err, -32769, 32768);
    parse_integer_test!(i32, parse_i32_err, -2147483649_i64, 2147483648_i64);
    parse_integer_test!(i64, parse_i64_err);
    parse_integer_test!(u8, parse_u8_err, -1, 257);
    parse_integer_test!(u16, parse_u16_err, -1, 65537);
    parse_integer_test!(u32, parse_u32_err, -1, 4294967297_u64);
    parse_integer_test!(u64, parse_u64_err, -1);
}
