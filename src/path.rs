//! # An Entity Path
//!
//! ## What is a path in Tocco?
//!
//! A path is used reference a relation or field within Tocco.
//!
//! ### Examples
//!
//! Field *username* on entity *Principal*:
//!
//! Principal.username
//!
//! The same field but starting at entity *User* and following the
//! relation *relPrincipal*:
//!
//! User.relPrincipal.username
//!
//! ## What is a Path?
//!
//! Apart from offering an easy way manipulate and work with
//! paths, `Path` ensure the path is syntactiacally correct.
//! However, no attempt is made to verify the target of the
//! `Path` exists.
//!
//! ### Usage Examples
//!
//! ```
//! use std::convert::TryFrom;
//! use tocco::path::PathBuf;
//!
//! let _path = PathBuf::try_from("User.relPrincipal.username").unwrap();
//! ```
//!
//! ## Safety - TQL-Injection
//!
//! Use [`crate::tql::TqlSafe`] when using paths in TQL.
use crate::tql::TqlSafe;
use std::cmp::Ordering;
use std::convert::TryFrom;
use std::fmt;
use std::iter::FusedIterator;
use std::ops::Deref;

/// Error returned when parsing of a path or element fails.
#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    /// Empty element or path
    Empty,

    /// Invalid character
    ///
    /// Character in element encountered that is not
    /// `A-Z`, `a-z` or `_`.
    InvalidCharacter,

    /// Field is not at end of path
    FieldNotAtEnd,

    /// Origin is not at start of path
    OriginNotAtStart,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Error::Empty => "Empty elment or path",
            Error::InvalidCharacter => "Invalid character",
            Error::FieldNotAtEnd => "Field is not last element in path",
            Error::OriginNotAtStart => "Origin is not first element of path",
        };
        write!(f, "{}", msg)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ElementKind {
    /// The path element is an origin.
    ///
    /// "User" is the origin in "User.relPrincipal.username."
    Origin,

    /// The path element is an relation.
    ///
    /// "relPrincipal" is the relation in path "User.relPrincipal.username".
    Relation,

    /// The path element is an origin.
    ///
    /// "username" is the field in "User.relPrincipal.username."
    Field,
}

impl ElementKind {
    /// Returns `true` if element is of kind origin.
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// assert!(Element::new("User").unwrap().kind().is_origin());
    /// ```
    pub fn is_origin(&self) -> bool {
        matches!(self, ElementKind::Origin)
    }

    /// Returns `true` if element is of kind relation.
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// assert!(Element::new("relPrincipal").unwrap().kind().is_relation());
    /// ```
    pub fn is_relation(&self) -> bool {
        matches!(self, ElementKind::Relation)
    }

    /// Returns `true` if element is of kind field.
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// assert!(Element::new("username").unwrap().kind().is_field());
    /// ```
    pub fn is_field(&self) -> bool {
        matches!(self, ElementKind::Field)
    }
}

fn validate_path(path: &str) -> Result<(), Error> {
    if path.is_empty() {
        return Err(Error::Empty);
    }

    let mut field_encountered = false;
    for (i, part) in path.split('.').enumerate() {
        let kind = Element::new(part)?.kind(); // Does syntax check for element.
        if field_encountered {
            return Err(Error::FieldNotAtEnd);
        }
        match kind {
            ElementKind::Field => field_encountered = true,
            ElementKind::Origin if i > 0 => {
                return Err(Error::OriginNotAtStart);
            }
            _ => {}
        }
    }
    Ok(())
}

/// A owned path
///
/// See also [module documentation](self).
#[derive(Clone, PartialEq, Eq)]
pub struct PathBuf {
    inner: String,
}

impl PathBuf {
    /// Create `PathBuf` from `str` without verification
    ///
    /// # Warning
    ///
    /// The caller **must** ensure the `path` is
    /// syntactically correct.
    #[must_use]
    fn new_unchecked(path: String) -> PathBuf {
        debug_assert!(validate_path(&path).is_ok());
        PathBuf { inner: path }
    }

    /// Create a new `PathBuf` from a `String`.
    ///
    /// ```
    /// use tocco::path::PathBuf;
    ///
    /// PathBuf::new("Principal.username".to_string()).unwrap();
    /// ```
    pub fn new(path: String) -> Result<Self, Error> {
        validate_path(&path)?;
        Ok(PathBuf::new_unchecked(path))
    }

    /// Consume `PathBuf` and return inner `String`
    ///
    /// ```
    /// use tocco::path::{ Path, PathBuf };
    ///
    /// let path_str = "Principal.username";
    /// let path = PathBuf::new(path_str.to_string()).unwrap();
    /// assert_eq!(path.into_string(), path_str);
    /// ```
    pub fn into_string(self) -> String {
        self.inner
    }

    /// Return inner path as `Path`.
    ///
    /// ```
    /// use tocco::path::{ Path, PathBuf };
    ///
    /// let path_str = "Principal.username";
    /// let path = PathBuf::new(path_str.to_string()).unwrap();
    /// assert_eq!(path.as_path(), Path::new(path_str).unwrap());
    /// ```
    pub fn as_path(&self) -> &Path {
        self
    }

    /// Append another `path` to `self`
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::{ Error, PathBuf };
    ///
    /// let mut path = PathBuf::try_from("User.relPrincipal").unwrap();
    /// path.push_str("relAdded.username").unwrap();
    ///
    /// assert_eq!(path.as_str(), "User.relPrincipal.relAdded.username");
    /// ```
    ///
    /// # Errors
    ///
    /// Pushing any element after a field is an error:
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::{ Error, PathBuf };
    ///
    /// let mut path = PathBuf::try_from("User.relPrincipal.username").unwrap();
    ///
    /// assert_eq!(path.push_str("second_field").unwrap_err(), Error::FieldNotAtEnd);
    /// assert_eq!(path.push_str("relAtion_after_field").unwrap_err(), Error::FieldNotAtEnd);
    /// ```
    ///
    /// Pushing an origin is an error:
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::{ Error, PathBuf };
    ///
    /// let mut path = PathBuf::try_from("User.relPrincipal").unwrap();
    ///
    /// assert_eq!(path.push_str("Second_origin").unwrap_err(), Error::OriginNotAtStart);
    /// ```
    pub fn push_str(&mut self, path: &str) -> Result<(), Error> {
        self.push_path(Path::new(path)?)
    }

    /// Add path to existing path
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// // path with origin
    /// let mut path = PathBuf::try_from("User").unwrap();
    /// let path2 = PathBuf::try_from("relPrincipal.username").unwrap();
    /// path.push_path(&path2);
    /// assert_eq!(path.as_str(), "User.relPrincipal.username");
    /// assert_eq!(path.get_element(2).unwrap().as_str(), "username");
    /// ```
    ///
    /// # Errors
    ///
    /// Pushing any element after a field is an error:
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::{ Error, PathBuf };
    ///
    /// let mut path = PathBuf::try_from("User.relPrincipal.username").unwrap();
    ///
    /// let path_to_add = PathBuf::try_from("relPrincipal.username").unwrap();
    /// assert_eq!(path.push_path(&path_to_add).unwrap_err(), Error::FieldNotAtEnd);
    ///
    /// let path_to_add = PathBuf::try_from("relPrincipal").unwrap();
    /// assert_eq!(path.push_path(&path_to_add).unwrap_err(), Error::FieldNotAtEnd);
    /// ```
    ///
    /// Pushing an absolute path (one starting with an origin) is an error:
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::{ Error, PathBuf };
    ///
    /// let mut path = PathBuf::try_from("User.relPrincipal").unwrap();

    /// let path_to_add = PathBuf::try_from("Another_origin").unwrap();
    /// assert_eq!(path.push_path(&path_to_add).unwrap_err(), Error::OriginNotAtStart);
    /// ```
    pub fn push_path(&mut self, path: &Path) -> Result<(), Error> {
        if path.is_absolute() {
            Err(Error::OriginNotAtStart)
        } else if self.has_field() {
            Err(Error::FieldNotAtEnd)
        } else {
            self.inner.push('.');
            self.inner.push_str(&path.inner);
            Ok(())
        }
    }
}

impl fmt::Debug for PathBuf {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.inner, f)
    }
}

impl TryFrom<&str> for PathBuf {
    type Error = Error;

    fn try_from(path: &str) -> Result<Self, Self::Error> {
        PathBuf::try_from(path.to_string())
    }
}

impl TryFrom<String> for PathBuf {
    type Error = Error;

    fn try_from(path: String) -> Result<Self, Self::Error> {
        validate_path(&path)?;
        Ok(PathBuf::new_unchecked(path))
    }
}

/// A slice of a path
///
/// See also [module documentation](self).
#[repr(transparent)]
#[derive(PartialEq, Eq)]
pub struct Path {
    inner: str,
}

#[allow(clippy::len_without_is_empty)] // can't be empty
impl Path {
    /// Create `PathBuf` from `str` without verification
    ///
    /// # Warning
    ///
    /// The caller **must** ensure the `path` is
    /// syntactically correct.
    #[must_use]
    fn new_unchecked(path: &str) -> &Path {
        debug_assert!(validate_path(path).is_ok());
        unsafe { &*(path as *const str as *const Path) }
    }

    /// Create a new `Path` from `&str`.
    ///
    /// ```
    /// use tocco::path::Path;
    ///
    /// Path::new("Principal.username").unwrap();
    /// ```
    pub fn new(path: &str) -> Result<&Self, Error> {
        validate_path(path)?;
        Ok(Path::new_unchecked(path))
    }

    /// Convert `Path` to an owned [`PathBuf`]
    ///
    /// ```
    /// use tocco::path::Path;
    ///
    /// let path_buf = Path::new("Principal.username").unwrap().to_path_buf();
    /// assert_eq!(path_buf, tocco::path::PathBuf::new("Principal.username".to_string()).unwrap());
    /// ```
    pub fn to_path_buf(&self) -> PathBuf {
        PathBuf::new_unchecked(self.inner.to_string())
    }

    /// Get inner `&str`.
    ///
    /// ```
    /// use tocco::path::Path;
    ///
    /// let path = Path::new("Principal.username").unwrap();
    /// assert_eq!(path.as_str(), "Principal.username");
    /// ```
    pub fn as_str(&self) -> &str {
        &self.inner
    }

    /// Check if path is absolute
    ///
    /// A path is considered absolute if at starts with an enitity name (`"User"`)
    /// rather than a relation name (`"relUser"`) or a field name (`"name"`).
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// // absolute
    /// assert!(PathBuf::try_from("Principal").unwrap().is_absolute());
    /// assert!(PathBuf::try_from("Principal.username").unwrap().is_absolute());
    /// assert!(PathBuf::try_from("Principal.relUser").unwrap().is_absolute());
    ///
    /// // relative
    /// assert!(!PathBuf::try_from("relPrincipal").unwrap().is_absolute());
    /// assert!(!PathBuf::try_from("relPrincipal.username").unwrap().is_absolute());
    /// assert!(!PathBuf::try_from("relPrincipal.relUser").unwrap().is_absolute());
    /// ```
    pub fn is_absolute(&self) -> bool {
        starts_with_origin(&self.inner)
    }

    /// Check if path contains a field
    ///
    /// Only the last element of the path may be a field.
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// // field
    /// assert!(PathBuf::try_from("username").unwrap().has_field());
    /// assert!(PathBuf::try_from("Principal.username").unwrap().has_field());
    /// assert!(PathBuf::try_from("relPrincipal.username").unwrap().has_field());
    /// assert!(PathBuf::try_from("User.relPrincipal.username").unwrap().has_field());
    ///
    /// // relation or origin
    /// assert!(!PathBuf::try_from("Principal").unwrap().has_field());
    /// assert!(!PathBuf::try_from("relPrincipal").unwrap().has_field());
    /// assert!(!PathBuf::try_from("relUser.relPrincipal").unwrap().has_field());
    /// ```
    pub fn has_field(&self) -> bool {
        self.last_element().kind().is_field()
    }

    /// The number of elements in path
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// assert_eq!(PathBuf::try_from("field").unwrap().element_count(), 1);
    /// assert_eq!(PathBuf::try_from("User.relPrincipal.username").unwrap().element_count(), 3);
    /// ```
    pub fn element_count(&self) -> usize {
        self.inner.as_bytes().iter().filter(|c| **c == b'.').count() + 1
    }

    /// Size of path in bytes
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// let path = "User.relPrincipal.username";
    /// assert_eq!(path.len(), 26);
    /// let path = PathBuf::try_from(path).unwrap();
    /// assert_eq!(path.len(), 26);
    /// ```
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    /// Fetch a specfic element of the path.
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// let path = PathBuf::try_from("Source.relAtion.relAnother.field").unwrap();
    /// assert_eq!(path.get_element(0).unwrap().as_str(), "Source");
    /// assert_eq!(path.get_element(1).unwrap().as_str(), "relAtion");
    /// assert_eq!(path.get_element(2).unwrap().as_str(), "relAnother");
    /// assert_eq!(path.get_element(3).unwrap().as_str(), "field");
    /// assert_eq!(path.get_element(4), None);
    /// ```
    pub fn get_element(&self, index: usize) -> Option<&Element> {
        self.inner
            .split('.')
            .nth(index)
            .map(|e| Element::new_unchecked(e))
    }

    /// Fetch first element of PathBuf
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// let path = PathBuf::try_from("User.relPrincipal.username").unwrap();
    /// assert_eq!(path.first_element().as_str(), "User");
    /// ```
    pub fn first_element(&self) -> &Element {
        Element::new_unchecked(self.inner.split('.').next().expect("empty path"))
    }

    /// Fetch last element of PathBuf
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// let path = PathBuf::try_from("User.relPrincipal.username").unwrap();
    /// assert_eq!(path.last_element().as_str(), "username");
    /// ```
    pub fn last_element(&self) -> &Element {
        Element::new_unchecked(self.inner.rsplit('.').next().expect("empty path"))
    }

    /// Return the origin of the path, if any
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// // path with origin
    /// assert_eq!(PathBuf::try_from("Principal").unwrap().get_origin().unwrap().as_str(), "Principal");
    /// assert_eq!(PathBuf::try_from("Principal.relUser").unwrap().get_origin().unwrap().as_str(), "Principal");
    /// assert_eq!(PathBuf::try_from("Principal.username").unwrap().get_origin().unwrap().as_str(), "Principal");
    ///
    /// // path without origin
    /// assert_eq!(PathBuf::try_from("relPrincipal").unwrap().get_origin(), None);
    /// assert_eq!(PathBuf::try_from("relPrincipal.relUser").unwrap().get_origin(), None);
    /// assert_eq!(PathBuf::try_from("username").unwrap().get_origin(), None);
    /// ```
    pub fn get_origin(&self) -> Option<&Element> {
        if starts_with_origin(&self.inner) {
            Some(self.first_element())
        } else {
            None
        }
    }

    /// Return the field of the path, if any
    ///
    /// ```
    /// use tocco::path::Path;
    ///
    /// // path with field
    /// assert_eq!(Path::new("username").unwrap().get_field().unwrap().as_str(), "username");
    /// assert_eq!(Path::new("Principal.username").unwrap().get_field().unwrap().as_str(), "username");
    /// assert_eq!(Path::new("User.relPrincipal.username").unwrap().get_field().unwrap().as_str(), "username");
    ///
    /// // path without field
    /// assert_eq!(Path::new("User").unwrap().get_field(), None);
    /// assert_eq!(Path::new("User.relPrincipal").unwrap().get_field(), None);
    /// ```
    pub fn get_field(&self) -> Option<&Element> {
        let last_element = self.last_element();
        if last_element.kind().is_field() {
            Some(last_element)
        } else {
            None
        }
    }

    /// Returns an iterator over the path elements.
    ///
    /// ```
    /// use std::convert::TryFrom;
    /// use tocco::path::PathBuf;
    ///
    /// let iter = PathBuf::try_from("Principal.relUser.username").unwrap();
    /// let mut iter = iter.elements();
    /// assert_eq!(iter.next().unwrap().as_str(), "Principal");
    /// assert_eq!(iter.next().unwrap().as_str(), "relUser");
    /// assert_eq!(iter.next().unwrap().as_str(), "username");
    /// assert_eq!(iter.next(), None);
    /// ```
    pub fn elements(&self) -> Iter {
        Iter {
            inner: self.inner.split('.'),
        }
    }

    /// Return an itorator over all relations in path
    ///
    /// ```
    /// use tocco::path::Path;
    ///
    /// assert_eq!(Path::new("Origin.field").unwrap().relations().next(), None);
    /// assert_eq!(Path::new("field").unwrap().relations().next(), None);
    /// assert_eq!(Path::new("Origin").unwrap().relations().next(), None);
    ///
    /// let relations: Vec<_> = Path::new("Origin.relAtion.field").unwrap().relations().map(|e| e.as_str()).collect();
    /// assert_eq!(relations, ["relAtion"]);
    ///
    /// let relations: Vec<_> = Path::new("Origin.relAtion1.relAtion2.field").unwrap().relations().map(|e| e.as_str()).collect();
    /// assert_eq!(relations, ["relAtion1", "relAtion2"]);
    ///
    /// let relations: Vec<_> = Path::new("relAtion").unwrap().relations().map(|e| e.as_str()).collect();
    /// assert_eq!(relations, ["relAtion"]);
    ///
    /// let relations: Vec<_> = Path::new("relAtion1.relAtion2").unwrap().relations().map(|e| e.as_str()).collect();
    /// assert_eq!(relations, ["relAtion1", "relAtion2"]);
    /// ```
    pub fn relations(&self) -> RelationIter {
        let mut elements = self.elements();
        if starts_with_origin(&self.inner) {
            let _origin = elements.next();
        }
        RelationIter { inner: elements }
    }
}

impl fmt::Debug for Path {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.inner, f)
    }
}

impl PartialOrd for Path {
    fn partial_cmp(&self, other: &Path) -> Option<Ordering> {
        let mut a = self.elements();
        let mut b = other.elements();

        loop {
            match (a.next(), b.next()) {
                (Some(a), Some(b)) => match PartialOrd::partial_cmp(a, b) {
                    Some(Ordering::Equal) => (),
                    v => break v,
                },
                (Some(_), None) => break Some(Ordering::Greater),
                (None, Some(_)) => break Some(Ordering::Less),
                (None, None) => break Some(Ordering::Equal),
            }
        }
    }
}

impl Ord for Path {
    fn cmp(&self, other: &Self) -> Ordering {
        PartialOrd::partial_cmp(self, other).unwrap()
    }
}

impl Deref for PathBuf {
    type Target = Path;

    fn deref(&self) -> &Path {
        Path::new_unchecked(&self.inner)
    }
}

fn validate_element(element: &str) -> Result<(), Error> {
    if element.is_empty() {
        Err(Error::Empty)
    } else if !element
        .as_bytes()
        .iter()
        .all(|c| c.is_ascii_alphanumeric() || *c == b'_')
    {
        Err(Error::InvalidCharacter)
    } else {
        Ok(())
    }
}

/// A owned path element.
pub struct ElementBuf {
    inner: String,
}

impl ElementBuf {
    /// Create `ElementBuf` from `str` without verification
    ///
    /// # Warning
    ///
    /// The caller **must** ensure the `path` is
    /// syntactically correct.
    #[must_use]
    fn new_unchecked(element: String) -> ElementBuf {
        debug_assert!(validate_element(&element).is_ok());
        ElementBuf { inner: element }
    }

    pub fn new(element: String) -> Result<ElementBuf, Error> {
        validate_element(&element)?;
        Ok(ElementBuf::new_unchecked(element))
    }
}

/// A slice of a path element.
///
/// In the path `"User.relPrincipal.username"`, `"User"`,
/// `"relPrincipal"` and `"username"` are elements.
#[repr(transparent)]
#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub struct Element {
    inner: str,
}

impl Element {
    /// Create `Element` from `str` without verification
    ///
    /// # Warning
    ///
    /// The caller **must** ensure the `path` is
    /// syntactically correct.
    #[must_use]
    fn new_unchecked(element: &str) -> &Self {
        debug_assert!(validate_element(element).is_ok());
        unsafe { &*(element as *const str as *const Element) }
    }

    /// Create new element
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// Element::new("username").unwrap();
    /// ```
    ///
    /// A single element must be specified. Specfying multiple
    /// elements results in a [`Error::InvalidCharacter`].
    ///
    /// ```
    /// use tocco::path::{ Element, Error };
    ///
    /// assert_eq!(Element::new("Principal.username").unwrap_err(), Error::InvalidCharacter);
    /// ```
    pub fn new(element: &str) -> Result<&Self, Error> {
        validate_element(element)?;
        Ok(Self::new_unchecked(element))
    }

    /// Get inner &str
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// let element = Element::new("username").unwrap();
    /// assert_eq!(element.as_str(), "username");
    /// ```
    pub fn as_str(&self) -> &str {
        &self.inner
    }

    /// Determine kind of element.
    ///
    /// ```
    /// use tocco::path::{ Element, ElementKind };
    ///
    /// assert_eq!(Element::new("User").unwrap().kind(), ElementKind::Origin);
    /// assert_eq!(Element::new("relPrincipal").unwrap().kind(), ElementKind::Relation);
    /// assert_eq!(Element::new("username").unwrap().kind(), ElementKind::Field);
    /// ```
    pub fn kind(&self) -> ElementKind {
        if starts_with_relation(&self.inner) {
            ElementKind::Relation
        } else if starts_with_origin(&self.inner) {
            ElementKind::Origin
        } else {
            ElementKind::Field
        }
    }

    /// Convert to `ElementBuf`
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// let buf = Element::new("username").unwrap().to_element_buf();
    /// assert_eq!(buf.as_str(), "username");
    /// ```
    pub fn to_element_buf(&self) -> ElementBuf {
        ElementBuf::new_unchecked(self.inner.to_string())
    }

    /// Convert to `PathBuf`
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// let buf = Element::new("username").unwrap().to_path_buf();
    /// assert_eq!(buf.as_str(), "username");
    /// ```
    pub fn to_path_buf(&self) -> PathBuf {
        PathBuf::new_unchecked(self.inner.to_string())
    }

    /// Convert to `Path`
    ///
    /// ```
    /// use tocco::path::Element;
    ///
    /// let buf = Element::new("username").unwrap().as_path();
    /// assert_eq!(buf.as_str(), "username");
    /// ```
    pub fn as_path(&self) -> &Path {
        Path::new_unchecked(&self.inner)
    }
}

impl fmt::Debug for Element {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.inner, f)
    }
}

impl Deref for ElementBuf {
    type Target = Element;

    fn deref(&self) -> &Element {
        Element::new_unchecked(&self.inner)
    }
}

/// Iterator over `PathBuf` elements.
///
/// See [`Path::elements()`]
pub struct Iter<'a> {
    inner: ::std::str::Split<'a, char>,
}

impl<'a> Iterator for Iter<'a> {
    type Item = &'a Element;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|e| Element::new_unchecked(e))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (1, None)
    }
}

impl FusedIterator for Iter<'_> {}

/// Iterator over all relations in Path
///
/// See [`Path::relations()`]
pub struct RelationIter<'a> {
    inner: Iter<'a>,
}

impl<'a> Iterator for RelationIter<'a> {
    type Item = &'a Element;

    fn next(&mut self) -> Option<Self::Item> {
        match self.inner.next() {
            Some(e) if e.kind().is_relation() => Some(e),
            _ => None,
        }
    }
}

impl FusedIterator for RelationIter<'_> {}

/// Quote path for safe use in TQL
///
/// ```
/// use std::convert::TryFrom;
/// use tocco::path::PathBuf;
/// use tocco::tql::TqlSafe;
///
/// let path = PathBuf::try_from("Principal.username").unwrap();
/// assert_eq!(path.tql_safe_string(), "'Principal'.'username'");
/// ```
impl TqlSafe for Path {
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        // Path is already sanitised and can't contain anything
        // dangerous like quotes. Quoting the whole path to ensure
        // it's not interpreted as keyword or literal should, thus,
        // be enough.
        let mut elements = self.elements();
        writer.write_char('\'')?;
        writer.write_str(
            elements
                .next()
                .expect("no first element in path") // Path always has at least one element
                .as_str(),
        )?;
        for element in elements {
            writer.write_str("'.'")?;
            writer.write_str(element.as_str())?;
        }
        writer.write_char('\'')
    }
}

/// Check if path starts with an origin
fn starts_with_origin(path: &str) -> bool {
    path.as_bytes()[0].is_ascii_uppercase()
}

/// Check if path starts with a relation
fn starts_with_relation(path: &str) -> bool {
    path.starts_with("rel")
        && path
            .as_bytes()
            .get(3)
            .map(|c| c.is_ascii_uppercase())
            .unwrap_or(false)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn element_ok() {
        assert_eq!(Element::new("rel").unwrap().kind(), ElementKind::Field);
        assert_eq!(Element::new("relative").unwrap().kind(), ElementKind::Field);
        assert_eq!(Element::new("rel1").unwrap().kind(), ElementKind::Field);
        assert_eq!(Element::new("Rel").unwrap().kind(), ElementKind::Origin);
        assert_eq!(Element::new("relA").unwrap().kind(), ElementKind::Relation);
        assert_eq!(
            Element::new("_nice_timestamp").unwrap().kind(),
            ElementKind::Field
        );
    }

    #[test]
    fn element_err() {
        assert_eq!(Element::new("").unwrap_err(), Error::Empty);
        assert_eq!(Element::new(".").unwrap_err(), Error::InvalidCharacter);
        assert_eq!(
            Element::new("Principal.username").unwrap_err(),
            Error::InvalidCharacter
        );
        assert_eq!(
            Element::new("relAbc.").unwrap_err(),
            Error::InvalidCharacter
        );
        assert_eq!(
            Element::new(".relAbc").unwrap_err(),
            Error::InvalidCharacter
        );
        assert_eq!(
            Element::new("relAbc.Abc").unwrap_err(),
            Error::InvalidCharacter
        );
        assert_eq!(Element::new("äbc").unwrap_err(), Error::InvalidCharacter);
        assert_eq!(
            Element::new("relAb/abc").unwrap_err(),
            Error::InvalidCharacter
        );
    }

    #[test]
    fn paths() {
        // valid paths
        let paths = &[
            "field",
            "relAtion",
            "Origin",
            "Origin.relAtion.relAtion_again",
            "Origin.relAtion.relAtion_again.a_field",
            "relAtion.relAtion_once_again",
            "relAtion.relAtion_once_again.some_field",
            "An_origin.field",
        ];
        for path in paths {
            assert!(Path::new(path).is_ok());
        }

        // invalid paths
        let paths = &[
            ("", Error::Empty),
            (".", Error::Empty),
            ("Origin.", Error::Empty),
            (".field", Error::Empty),
            ("Origin..field", Error::Empty),
            ("Origin.relInvali$.field", Error::InvalidCharacter),
            ("Origin.Origin", Error::OriginNotAtStart),
            ("field.relAtion", Error::FieldNotAtEnd),
            ("field.Origin", Error::FieldNotAtEnd),
        ];
        for (path, error) in paths {
            assert_eq!(&Path::new(path).unwrap_err(), error);
        }
    }

    #[test]
    fn try_from_ok() {
        PathBuf::try_from("abcdefghiklmnopqrstuvxyz").unwrap();
        PathBuf::try_from("Abc.abcdefghiklmnopqrstuvxyz").unwrap();
        PathBuf::try_from("Abc.relDef.abcdefghiklmnopqrstuvxyz").unwrap();
        PathBuf::try_from("relDef.abcdefghiklmnopqrstuvxyz").unwrap();
        PathBuf::try_from("relDef.abc_def_ghi0123456789").unwrap();
        PathBuf::try_from(String::from_utf8(vec![b'a'; 255]).unwrap()).unwrap();
    }

    #[test]
    fn try_from_err() {
        assert_eq!(PathBuf::try_from("").unwrap_err(), Error::Empty);
        assert_eq!(PathBuf::try_from(".").unwrap_err(), Error::Empty);
        assert_eq!(PathBuf::try_from("relAbc..abc").unwrap_err(), Error::Empty);
        assert_eq!(PathBuf::try_from("relAbc.").unwrap_err(), Error::Empty);
        assert_eq!(PathBuf::try_from(".relAbc").unwrap_err(), Error::Empty);
        assert_eq!(
            PathBuf::try_from("relAbc.Abc").unwrap_err(),
            Error::OriginNotAtStart
        );
        assert_eq!(
            PathBuf::try_from("relAbc.abc.abc").unwrap_err(),
            Error::FieldNotAtEnd
        );
        assert_eq!(
            PathBuf::try_from("relAbc.äbc").unwrap_err(),
            Error::InvalidCharacter
        );
        assert_eq!(
            PathBuf::try_from("relAb/.abc").unwrap_err(),
            Error::InvalidCharacter
        );
    }

    #[test]
    fn is_absolute() {
        // absolute
        assert!(PathBuf::try_from("Abc").unwrap().is_absolute());
        assert!(PathBuf::try_from("Abc.relAbc").unwrap().is_absolute());
        assert!(PathBuf::try_from("Abc.relAbc.abc").unwrap().is_absolute());

        // relative
        assert!(!PathBuf::try_from("relAbc").unwrap().is_absolute());
        assert!(!PathBuf::try_from("relAbc.abc").unwrap().is_absolute());
        assert!(!PathBuf::try_from("abc").unwrap().is_absolute());
    }

    #[test]
    fn has_field() {
        // has field
        assert!(PathBuf::try_from("Source.field").unwrap().has_field());
        assert!(PathBuf::try_from("Source.relAtion.field")
            .unwrap()
            .has_field());
        assert!(PathBuf::try_from("relAtion.field").unwrap().has_field());

        // has no field
        assert!(!PathBuf::try_from("Source").unwrap().has_field());
        assert!(!PathBuf::try_from("RelAtion").unwrap().has_field());
    }

    #[test]
    fn element_count() {
        assert_eq!(PathBuf::try_from("Source").unwrap().element_count(), 1);
        assert_eq!(
            PathBuf::try_from("Source.field").unwrap().element_count(),
            2
        );
        assert_eq!(
            PathBuf::try_from("Source.relAtion.field")
                .unwrap()
                .element_count(),
            3
        );
        assert_eq!(
            PathBuf::try_from("Source.relAtion.relAnother.field")
                .unwrap()
                .element_count(),
            4
        );
        assert_eq!(
            PathBuf::try_from("relAtion.field").unwrap().element_count(),
            2
        );
    }

    #[test]
    fn push() {
        let mut path = PathBuf::try_from("User").unwrap();
        path.push_str("relRegistration").unwrap();
        path.push_str("relEvent").unwrap();
        path.push_str("unique_id").unwrap();
        assert_eq!(path.as_str(), "User.relRegistration.relEvent.unique_id");
        assert_eq!(
            path.push_str("Origin").unwrap_err(),
            Error::OriginNotAtStart,
        );
        assert_eq!(path.push_str("").unwrap_err(), Error::Empty);

        let mut path = PathBuf::try_from("field").unwrap();
        assert_eq!(path.push_str("field").unwrap_err(), Error::FieldNotAtEnd);
        assert_eq!(
            path.push_str("Origin").unwrap_err(),
            Error::OriginNotAtStart
        );

        let mut path = PathBuf::try_from("relAtion").unwrap();
        assert_eq!(
            path.push_str("Origin").unwrap_err(),
            Error::OriginNotAtStart
        );
        path.push_str("field").unwrap();
        assert_eq!(path.as_str(), "relAtion.field");
    }

    #[test]
    fn push_path() {
        let path = Path::new("relUser").unwrap();
        let relation_path = PathBuf::try_from("relPrincipal").unwrap();
        let field_path = PathBuf::try_from("relPrincipal.username").unwrap();
        let origin_path = PathBuf::try_from("User").unwrap();

        let mut path2 = path.to_path_buf();
        assert!(path2.push_path(&relation_path).is_ok());
        assert!(path2.push_path(&field_path).is_ok());
        assert_eq!(
            path2.push_path(&relation_path).unwrap_err(),
            Error::FieldNotAtEnd
        );
        assert_eq!(
            path2.push_path(&field_path).unwrap_err(),
            Error::FieldNotAtEnd
        );

        assert_eq!(
            path.to_path_buf().push_path(&origin_path).unwrap_err(),
            Error::OriginNotAtStart
        );
    }

    #[test]
    fn element_kind() {
        // fields
        assert!(Element::new("rel_ationbutnotreally")
            .unwrap()
            .kind()
            .is_field());
        assert!(Element::new("field").unwrap().kind().is_field());

        // relations
        assert!(Element::new("relName").unwrap().kind().is_relation());
        assert!(Element::new("relComplex_name")
            .unwrap()
            .kind()
            .is_relation());

        // origins
        assert!(Element::new("Origin_name").unwrap().kind().is_origin());
    }

    #[test]
    fn tql_quoted_path() {
        let path = Path::new("single_element").unwrap();
        assert_eq!(path.tql_safe_string(), "'single_element'");

        let path = Path::new("One.relTwo.three").unwrap();
        assert_eq!(path.tql_safe_string(), "'One'.'relTwo'.'three'");
    }

    #[test]
    fn path_ordering() {
        let p = |s| Path::new(s).unwrap();

        assert_eq!(p("Abc"), p("Abc"));
        assert_eq!(p("Abc.def"), p("Abc.def"));

        let ordered = &[
            "A",
            "Abc",
            "Abc.a_c",
            "Abc.abc",
            "Abc.relA",
            "Abc.relA.b",
            "Abc.rela",
            "Abc.x",
            "abb",
            "abc",
            "abd",
        ];
        for items in ordered.windows(2) {
            let (a, b) = (items[0], items[1]);
            assert!(p(a) < p(b), "{} < {}", a, b);
            assert!(p(b) > p(a), "{} > {}", b, a);
        }
    }
}
