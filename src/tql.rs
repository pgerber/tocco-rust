//! # Tocco Query Language (TQL)
use crate::path::PathBuf;
#[cfg(feature = "chrono")]
use chrono::{DateTime, Duration, NaiveDate, NaiveTime, TimeZone, Utc};
use std::fmt;

/// Generate an `"IN(path, item1, item2)"` TQL expression with all elements
/// properly quoted and escaped.
///
/// ```
/// use std::convert::TryFrom;
/// use tocco::path::PathBuf;
/// use tocco::tql::in_expr;
///
/// let path = PathBuf::try_from("relPrincipal.failed_logins").unwrap();
/// let items = &["arya", "hazel", "piper"];
/// let mut expr = String::new();
/// in_expr(&mut expr, &path, &mut items.iter()).unwrap();
/// assert_eq!(expr, r#"IN('relPrincipal'.'failed_logins', "arya", "hazel", "piper")"#);
/// ```
///
/// # Empty Iterator
///
/// Nothing is written to `writer` if the Iterator yields no item:
///
/// ```
/// use std::convert::TryFrom;
/// use std::fmt;
/// use tocco::path::PathBuf;
/// use tocco::tql::in_expr;
///
/// let path = PathBuf::try_from("relPrincipal.failed_logins").unwrap();
/// let mut expr = String::new();
/// assert!(in_expr(&mut expr, &path, &mut std::iter::empty::<&&str>()).is_ok());
/// assert!(expr.is_empty());
/// ```
///
pub fn in_expr<'a, I, T, W>(writer: &mut W, path: &PathBuf, items: &mut I) -> fmt::Result
where
    I: Iterator<Item = &'a T>,
    T: TqlSafe + 'a,
    W: fmt::Write,
{
    match items.next() {
        Some(first_item) => {
            writer.write_str("IN(")?;
            path.tql_safe(writer)?;
            writer.write_str(", ")?;
            first_item.tql_safe(writer)?;
            for item in items {
                writer.write_str(", ")?;
                item.tql_safe(writer)?;
            }
            writer.write_char(')')
        }
        None => Ok(()),
    }
}

/// Like [`in_expr`] but returns a [`String`]
///
/// ```
/// use std::convert::TryFrom;
/// use tocco::tql::in_expr_string;
/// use tocco::path::PathBuf;
///
/// let path = PathBuf::try_from("relPrincipal.failed_logins").unwrap();
/// assert_eq!(
///     in_expr_string(&path, &mut [1_u64, 5, 10].iter()).unwrap().unwrap(),
///     "IN('relPrincipal'.'failed_logins', 1, 5, 10)",
/// );
/// ```
///
/// # Empty Iterator
///
/// [`None`] is returned if the iterator yields no items:
///
/// ```
/// use std::convert::TryFrom;
/// use tocco::tql::in_expr_string;
/// use tocco::path::PathBuf;
///
/// let path = PathBuf::try_from("relPrincipal.failed_logins").unwrap();
/// assert!(in_expr_string(&path, &mut std::iter::empty::<&u64>()).unwrap().is_none());
/// ```
pub fn in_expr_string<'a, I, T>(path: &PathBuf, items: &mut I) -> Result<Option<String>, fmt::Error>
where
    I: Iterator<Item = &'a T>,
    T: TqlSafe + 'a,
{
    let mut buffer = String::new();
    in_expr(&mut buffer, path, items)?;
    if buffer.is_empty() {
        Ok(None)
    } else {
        Ok(Some(buffer))
    }
}

/// Injection-safe quoting for use in TQL.
///
/// # Example
///
/// ```
/// use std::fmt;
/// use tocco::tql::TqlSafe;
///
/// struct PhoneNumber {
///     e164: String,
/// }
///
/// impl TqlSafe for PhoneNumber {
///     fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
///     where
///         W: fmt::Write
///     {
///         writer.write_char('"')?;
///         writer.write_str(&self.e164)?;
///         writer.write_char('"')
///     }
/// }
///
/// let number = PhoneNumber { e164: "+41 44 123 11 22".to_string() };
/// let expected_result = r#""+41 44 123 11 22""#;
///
/// // Convert via `fmt::Write` trait
///
/// let mut result = String::new();
/// number.tql_safe(&mut result).unwrap();
/// assert_eq!(result, expected_result);
///
/// // Direct conversion to `String`
///
/// assert_eq!(number.tql_safe_string(), expected_result);
/// ```
pub trait TqlSafe {
    /// Quote for safe use in TQL
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write;

    /// String version of [`TqlSafe::tql_safe`]
    fn tql_safe_string(&self) -> String {
        let mut buffer = String::new();
        self.tql_safe(&mut buffer)
            .expect("write failure while quoting");
        buffer
    }
}

fn write_escaped_java_char<W>(writer: &mut W, character: char) -> fmt::Result
where
    W: fmt::Write,
{
    match character {
        '\n' => writer.write_str(r#"\n"#),
        '\r' => writer.write_str(r#"\r"#),
        '\\' => writer.write_str(r#"\\"#),
        '"' => writer.write_str(r#"\""#),
        c => writer.write_char(c),
    }
}

/// Injection-safe representation of a &str for use in TQL
///
/// ```
/// use tocco::tql::TqlSafe;
///
/// let raw = "Jane shouted, \"go left\",\n but Joe went left anyway.";
/// let quoted = r#""Jane shouted, \"go left\",\n but Joe went left anyway.""#;
/// assert_eq!(raw.tql_safe_string(), quoted);
/// ```
impl TqlSafe for &str {
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        writer.write_char('"')?;
        for c in self.chars() {
            write_escaped_java_char(writer, c)?;
        }
        writer.write_char('"')
    }
}

/// Injection-safe representation of an `Option<_>`  for use in TQL.
///
/// ```
/// use tocco::tql::TqlSafe;
///
/// let none: Option<&str> = None;
/// assert_eq!(none.tql_safe_string(), "null");
///
/// let some = Some("line 1\nline 2");
/// assert_eq!(some.tql_safe_string(), r#""line 1\nline 2""#);
/// ```
impl<T> TqlSafe for Option<T>
where
    T: TqlSafe,
{
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        match self {
            Some(value) => value.tql_safe(writer),
            None => writer.write_str("null"),
        }
    }
}

macro_rules! tql_safe_impl {
    ($type_name:ident, $test_name:ident, $value:expr, $safe_repr:expr) => {
        /// Injection-safe representation for use in TQL.
        ///
        /// ```
        /// use tocco::tql::TqlSafe;
        ///
        #[doc = concat!("let safe_repr = TqlSafe::tql_safe_string(&", stringify!($value), ");") ]
        #[doc = concat!("assert_eq!(safe_repr, ", stringify!($safe_repr), ");") ]
        /// ```
        impl TqlSafe for $type_name {
            fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
            where
                W: fmt::Write,
            {
                write!(writer, "{}", self)
            }
        }

        #[test]
        fn $test_name() {
            let mut safe_repr = String::new();
            TqlSafe::tql_safe(&$value, &mut safe_repr).unwrap();
            assert_eq!(safe_repr, $safe_repr);
        }
    };
}

tql_safe_impl!(bool, test_tql_impl_bool, false, "false");
tql_safe_impl!(u8, test_tql_impl_u8, 15, "15");
tql_safe_impl!(u16, test_tql_impl_u16, 15, "15");
tql_safe_impl!(u32, test_tql_impl_u32, 15, "15");
tql_safe_impl!(u64, test_tql_impl_u64, 15, "15");
tql_safe_impl!(i8, test_tql_impl_i8, -15, "-15");
tql_safe_impl!(i16, test_tql_impl_i16, -15, "-15");
tql_safe_impl!(i32, test_tql_impl_i32, -15, "-15");
tql_safe_impl!(i64, test_tql_impl_i64, -15, "-15");
tql_safe_impl!(usize, test_tql_impl_usize, 15, "15");
tql_safe_impl!(isize, test_tql_impl_isize, -15, "-15");
tql_safe_impl!(f32, test_tql_impl_f32, 15.85, "15.85");
tql_safe_impl!(f64, test_tql_impl_f64, -15e-15, "-0.000000000000015");

/// Injection-safe representation of a datetime for use in TQL.
///
/// The time is converted to UTC and the client is expected to
/// set the timezone accordingly.
///
/// ```
/// use chrono::DateTime;
/// use tocco::tql::TqlSafe;
///
/// let datetime = DateTime::parse_from_rfc3339("2021-01-01T14:12:18+03:00").unwrap();
/// assert_eq!(datetime.tql_safe_string(), r#"datetime:"2021-01-01 11:12:18""#);
///
/// let datetime = DateTime::parse_from_rfc3339("2021-01-01T14:12:18.123+03:00").unwrap();
/// assert_eq!(datetime.tql_safe_string(), r#"datetime:"2021-01-01 11:12:18.123""#);
/// ```
#[cfg(feature = "chrono")]
impl<T> TqlSafe for DateTime<T>
where
    T: TimeZone,
{
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        write!(
            writer,
            "{}",
            self.with_timezone(&Utc)
                .format(r#"datetime:"%Y-%m-%d %H:%M:%S%.f""#)
        )
    }
}

/// Injection-safe representation of a naive time for use in TQL.
///
/// ```
/// use chrono::NaiveTime;
/// use tocco::tql::TqlSafe;
///
/// let date = NaiveTime::from_hms(18, 52, 26);
/// assert_eq!(date.tql_safe_string(), r#"time:"18:52:26""#);
///
/// let date = NaiveTime::from_hms_milli(18, 52, 26, 58);
/// assert_eq!(date.tql_safe_string(), r#"time:"18:52:26.058""#);
/// ```
#[cfg(feature = "chrono")]
impl TqlSafe for NaiveTime {
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        write!(writer, "{}", self.format(r#"time:"%H:%M:%S%.f""#))
    }
}

/// Injection-safe representation of a naive date for use in TQL.
///
/// ```
/// use chrono::NaiveDate;
/// use tocco::tql::TqlSafe;
///
/// let date = NaiveDate::from_ymd(2022, 05, 06);
/// assert_eq!(date.tql_safe_string(), r#"date:"2022-05-06""#);
/// ```
#[cfg(feature = "chrono")]
impl TqlSafe for NaiveDate {
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        write!(writer, "{}", self.format(r#"date:"%Y-%m-%d""#))
    }
}

/// Injection-safe representation of a duration for use in TQL
///
/// ```
/// use chrono::Duration;
/// use tocco::tql::TqlSafe;
///
/// let duration = Duration::milliseconds(185648369);
/// assert_eq!(duration.tql_safe_string(), "185648369");
/// ```
///
/// # Implementation Note
///
/// This is converted to milliseconds. It's also possible to
/// represent this as *duration:"HH:MM:SS"* but it's not
/// possible to represent negative duration with that
/// syntax.
#[cfg(feature = "chrono")]
impl TqlSafe for Duration {
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        // let millis = self.num_milliseconds();
        // if millis < 0 {
        //     return Err(fmt::Error);
        // }
        // write!(
        //     writer,
        //     r#"duration:"{}""#,
        //     self.num_milliseconds()
        // )
        write!(writer, "{}", self.num_milliseconds())
    }
}

/// Injection-safe representation of a duration for use in TQL
///
/// ```
/// use std::time::Duration;
/// use tocco::tql::TqlSafe;
///
/// let duration = Duration::from_millis(185648369);
/// assert_eq!(duration.tql_safe_string(), "185648369");
/// ```
impl TqlSafe for std::time::Duration {
    fn tql_safe<W>(&self, writer: &mut W) -> fmt::Result
    where
        W: fmt::Write,
    {
        write!(writer, "{}", self.as_millis())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_escaping() {
        assert_eq!(TqlSafe::tql_safe_string(&r#""#), r#""""#);
        assert_eq!(TqlSafe::tql_safe_string(&r#"abc"#), r#""abc""#);
        assert_eq!(TqlSafe::tql_safe_string(&"'\"!:\n\rq"), r#""'\"!:\n\rq""#);
        assert_eq!(TqlSafe::tql_safe_string(&r#"\n"#), r#""\\n""#);
        assert_eq!(TqlSafe::tql_safe_string(&r#"" ""#), r#""\" \"""#);
    }
}
