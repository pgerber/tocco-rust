//! # Tocco Client Library for Rust
//!
//! ...
//!
//! ## Character Encoding
//!
//! The database, Postgres, will not accept any strings containing
//! a NUL character. Expect request containing a NUL character to
//! fail.  No attemptis made, within this library, to prevent NUL
//! from being sent.
//!
//! See [Character Types](https://www.postgresql.org/docs/12/datatype-character.html)
//! in Postgres's documentation.

#![allow(clippy::redundant_closure)]

pub mod path;
pub mod tql;
pub mod types;
